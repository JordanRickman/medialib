#/usr/local/bin/python3
"""MediaLib main command line program.
This program is the main executable for MediaLib. It processes command line
arguments and calls the requisite functions from the media_lib package."""

COPYRIGHT_DOC = """MediaLib version 0, Copyright (C) 2013  Jordan Rickman
MediaLib comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions; type `medialib license' for details."""

##### IMPORTS #####
import sys


##### MAIN CODE #####
if len(sys.argv) < 2 :
    print(COPYRIGHT_DOC)
    print("\n\nSorry, medialib is in pre-alpha and not yet usable.")

elif sys.argv[1] == 'license':
    license = open('LICENSE', 'r')
    for line in license:
        print(line, end='')
