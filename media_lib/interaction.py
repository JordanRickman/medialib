"""This module provides methods to print information and prompt the user.
It has functions print information at various levels: ERROR, WARNING,
INFO, and VERBOSE.
It has functions to prompt the user for a string input and to prompt the user
for binary response questions."""


##### IMPORTS #####
import sys
import io
import re


##### CONSTANTS #####
LVL_ERROR = "ERROR"
LVL_WARN = "WARNING"
LVL_INFO = "INFO"
LVL_VERBOSE = "VERBOSE"


##### CLASSES #####
class Logger:
    """An object that handles logging at various logging levels.
    It holds references to a log file and an error file. (e.g. stdout, stderr)
    Messages of level ERROR are always logged to the error file.
    Messages of level WARNING are logged to the error file unless the program
    is in silent mode.
    Messages of level INFO are logged to the log file unless the program is in
    silent mode.
    Messages of level VERBOSE are logged to the log file only if the program is
    in verbose mode."""
    _logfile = sys.stdout
    _errfile = sys.stderr
    
    def __init__(self, logfile=None, errfile=None, append=False):
        if logfile == None:
            self._logfile = sys.stdout
        elif isinstance(logfile, io.IOBase):
            self._logfile = logfile
        elif isinstance(logfile, str):
            # IOErrors may be raised here
            if append:
                self._logfile = open(logfile, 'wa')
            else:
                self._logfile = open(logfile, 'w')
        else:
            raise TypeError("Log file must be a file object or string.")
        
        if errfile == None:
            self._errfile = sys.stderr
        elif isinstance(errfile, io.IOBase):
            self._errfile = errfile
        elif isinstance(logfile, str):
            # IOErrors may be raised here
            if append:
                self._errfile = open(errfile, 'wa')
            else:
                self._errfile = open(errfile, 'w')
        else:
            raise TypeError("Error file must be a file object or string.")
        
    
    def log(self, message, level=LVL_INFO):
        if level == LVL_INFO: #TODO: and not in silent mode
            self._logfile.write(str(message) + '\n')
        elif level == LVL_VERBOSE: #TODO: and in verbose mode
            self._logfile.write(str(message) + '\n')
        elif level == LVL_WARN: #TODO: and not in silent mode
            self._errfile.write(str(message) + '\n')
        elif level == LVL_ERROR:
            self._errfile.write(str(message) + '\n')
        else:
            raise ValueError("Invalid logging level: " + str(level))


##### FUNCTIONS #####
def prompt(message, requireResponse=True):
    """Prompt for an answer to a question, returning a string.
    Unless you call prompt(message, False), will keep asking again if the user
    gives a blank response."""
    s = input(message)
    if requireResponse:
        while len(s.strip()) == 0:
            s = input(message)
    return s

def binary_prompt(message, trueRegEx='[yY].*', falseRegEx='[nN].*'):
    """Prompt for an answer to a yes or no question, returning a boolean.
    Though the standard responses are y or Y for true and n or N for false, you
    may pass reqular expression arguments that determine what counts as a true
    answer and what as a false answer.
    If the user gives an empty response or an invalid response, prompts the user
    again.
    False is prioritized over true, so if a response fits both falseRegEx and
    trueRegEx, it will be treated as false."""
    if (not isinstance(trueRegEx, str)) or (not isinstance(falseRegEx, str)):
        raise TypeError("Regular expressions for responses must be str objects.")
    
    while True:
        s = input(message)
        # Must have some response
        while len(s.strip() == 0):
            s = input(message)
        # Prioritize false over true
        if re.match(falseRegEx, s) != None:
            return False
        elif re.match(trueRegEx, s) != None:
            return True
        # If doesn't match false or true, try again
        else:
            print("Invalid answer. True answer must match " + trueRegEx + 
                  ", false answer must match " + falseRegEx)
            continue

def log(message, level=LVL_INFO):
    """Log a message to the default output.
    The second argument should be one of ERROR, WARNING, INFO, or VERBOSE.
    See the docstring for the Logger class."""
    _output.log(message, level)


##### INITIALIZATION #####
_output = Logger(sys.stdout, sys.stderr, False)
    